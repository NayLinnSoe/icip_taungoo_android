package com.zwenexsys.icipandroid.databaseInput;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.zwenexsys.icipandroid.databasePkg.DatabaseHandler;
import com.zwenexsys.icipandroid.databasePkg.DummyTable;
import com.zwenexsys.icipandroid.databasePkg.NotInDatabaseException;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mt on 5/20/17.
 */

public class InputDatabaseManager {

    private static InputDatabaseManager manager;
    private static SQLiteDatabase handler;

    public InputDatabaseManager() {
    }

    public static InputDatabaseManager initDatabaseManager() {
        if (manager == null)
            manager = new InputDatabaseManager();
        return manager;
    }

    //get input list from database
    public static List<InputTable> getInstances(Context context) {

        List<InputTable> list = new ArrayList<>();
        handler = new InputDatabaseHandler(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + InputTable.getTABLE_NAME();
        Cursor cursor = handler.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                String title = cursor.getString(1);
                String content = cursor.getString(2);
                list.add(new InputTable(title, content));
            } while (cursor.moveToNext());
        }
        return list;
    }

    public static List<String> getTitle(Context context) {

        List<String> list = new ArrayList<>();
        handler = new InputDatabaseHandler(context).getReadableDatabase();
        String selectQuery = "SELECT DISTINCT title FROM " + InputTable.getTABLE_NAME();
        Cursor cursor = handler.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                String title = cursor.getString(0);
                list.add(title);
            } while (cursor.moveToNext());
        }
        return list;
    }

    //insert input to database
    public static void saveInputTable(InputTable data, Context context) {
        handler = new InputDatabaseHandler(context).getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("title", data.getTitle());
        cv.put("content", data.getContent());

        handler.insert(InputTable.getTABLE_NAME(), null, cv);
        handler.close();
    }

    //delete all from table
    public static void deleteInputs(Context context) {
        handler = new InputDatabaseHandler(context).getWritableDatabase();
        String deleteQuery = "DELETE FROM " + InputTable.getTABLE_NAME();
        handler.execSQL(deleteQuery);

    }

}
