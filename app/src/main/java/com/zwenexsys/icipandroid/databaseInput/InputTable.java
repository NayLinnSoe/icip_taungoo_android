package com.zwenexsys.icipandroid.databaseInput;

/**
 * Created by mt on 5/20/17.
 */


public class InputTable {

    private static String TABLE_NAME = "input";
    private static String TABLE_COLUMN =
            "id integer primary key autoincrement, " +
                    "title text," +
                    "content text";

    private String title;
    private String content;

    public InputTable() {

    }

    public InputTable(String title, String content) {

        this.title = title;
        this.content = content;
    }

    public static String getTABLE_NAME() {
        return TABLE_NAME;
    }

    public static String getTableColumn() {
        return TABLE_COLUMN;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
