package com.zwenexsys.icipandroid.databaseInput;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.zwenexsys.icipandroid.databasePkg.DummyTable;

/**
 * Created by mt on 5/20/17.
 */

public class InputDatabaseHandler extends SQLiteOpenHelper {


    private static final String DB_NAME = "MovieRepo";
    private static final int DB_VERSION = 1;

    public InputDatabaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + InputTable.getTABLE_NAME() + "("
                + InputTable.getTableColumn() + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + InputTable.getTABLE_NAME());
        onCreate(db);
    }

}
