package com.zwenexsys.icipandroid.Session;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by nls on 10/10/16.
 */

public class SessionManager {

    private static final String USER_NAME = "user_email";
    private static final String SESSION_TOKEN = "session_token";
    private static final String USER_SESSION = "user_session";
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public static void setUserName(Context context, String email) {
        preferences = context.getSharedPreferences(USER_SESSION, context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(USER_NAME, email);
        editor.commit();
    }

    public static void setSessionToken(Context context, String token) {
        preferences = context.getSharedPreferences(USER_SESSION, context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(SESSION_TOKEN, token);
        editor.commit();
    }

    public static String getUserName(Context context) {
        preferences = context.getSharedPreferences(USER_SESSION, context.MODE_PRIVATE);
        editor = preferences.edit();
        String email = preferences.getString(USER_NAME, "");
        return email;
    }

    public static String getSessionToken(Context context) {
        preferences = context.getSharedPreferences(USER_SESSION, context.MODE_PRIVATE);
        editor = preferences.edit();
        String token = preferences.getString(SESSION_TOKEN, "");
        return token;
    }

    public static void resetSession(Context context) {
        preferences = context.getSharedPreferences(USER_SESSION, context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public static void resetData(Context context) {
        preferences = context.getSharedPreferences(USER_SESSION, context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
