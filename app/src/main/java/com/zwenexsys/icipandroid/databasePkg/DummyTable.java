package com.zwenexsys.icipandroid.databasePkg;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by invictus on 5/19/17.
 */

public class DummyTable {

    private static String TABLE_NAME = "dummy";
    private static String TABLE_COLUMN =
            "id integer primary key, " +
            "img blob not null," +
            "date string," +
            "check boolean," +
            "description text";
    private int id;
    private Bitmap img;
    private String date;
    private boolean check;
    private String description;

    public DummyTable() {

    }

    public DummyTable(int id, Bitmap img, String date, boolean check, String description) {
        this.id = id;
        this.img = img;
        this.date = date;
        this.check = check;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static String getTABLE_NAME() {
        return TABLE_NAME;
    }

    public static String getTableColumn() {
        return TABLE_COLUMN;
    }
}
