package com.zwenexsys.icipandroid.databasePkg;

/**
 * Created by invictus on 5/19/17.
 */

public class NotInDatabaseException extends Exception {

    public NotInDatabaseException() {
        super("No results found on searching the database.");
    }

}
