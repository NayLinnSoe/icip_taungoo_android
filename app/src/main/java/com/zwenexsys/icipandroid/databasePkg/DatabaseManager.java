package com.zwenexsys.icipandroid.databasePkg;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by invictus on 5/19/17.
 */

public class DatabaseManager {

    private static DatabaseManager manager;
    private static SQLiteDatabase handler;

    private DatabaseManager() {  }

    public static DatabaseManager initDatabaseManager() {
        if (manager==null)
        manager = new DatabaseManager();
        return manager;
    }

    public static List<DummyTable> getInstances(Context context) {

        List<DummyTable> list = new ArrayList<>();
        handler = new DatabaseHandler(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DummyTable.getTABLE_NAME();
        Cursor cursor = handler.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                int id = Integer.parseInt(cursor.getString(0));
                Bitmap image = byteToBitMap(cursor.getBlob(cursor.getColumnIndex("img")));
                String date = cursor.getString(2);
                boolean check = Boolean.parseBoolean(cursor.getString(3));
                String description = cursor.getString(4);
                list.add(new DummyTable(id, image, date, check, description));
            } while (cursor.moveToNext());
        }
        return list;
    }

    public static void saveDummyTable(DummyTable data, Context context){

        handler = new DatabaseHandler(context).getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id", data.getId() );
        cv.put("description", data.getDescription());
        cv.put("date", data.getDate());
        cv.put("check", data.isCheck());
        cv.put("img", BitMaptoBtye(data.getImg()));
        handler.insert(DummyTable.getTABLE_NAME(), null, cv);
        handler.close();
    }

    public static  DummyTable searchById(int id, Context context)throws NotInDatabaseException{
        handler = new DatabaseHandler(context).getReadableDatabase();
        Cursor cursor = handler.query(DummyTable.getTABLE_NAME(), new String[] { "id", "img", "date", "check", "description" },
                "id" + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();

            Bitmap image = byteToBitMap(cursor.getBlob(cursor.getColumnIndex("img")));
            String date = cursor.getString(2);
            boolean check = Boolean.parseBoolean(cursor.getString(3));
            String description = cursor.getString(4);

            return new DummyTable(id, image, date, check, description);
        }
            throw new NotInDatabaseException();
    }

    public static  DummyTable searchByDescription(String description, Context context)throws NotInDatabaseException{
        handler = new DatabaseHandler(context).getReadableDatabase();
        Cursor cursor = handler.query(DummyTable.getTABLE_NAME(), new String[] { "id", "img", "date", "check", "description" },
                "description" + " LIKE ",
                new String[] { "%" + description +"%" }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int dId = Integer.parseInt(cursor.getString(0));
            Bitmap image = byteToBitMap(cursor.getBlob(cursor.getColumnIndex("img")));
            String date = cursor.getString(2);
            boolean check = Boolean.parseBoolean(cursor.getString(3));

            return new DummyTable(dId, image, date, check, description);
        }
        throw new NotInDatabaseException();
    }

    public static void updateDummyTable(DummyTable data, Context context) {
        handler = new DatabaseHandler(context).getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("description", data.getDescription());
        cv.put("date", data.getDate());
        cv.put("check", data.isCheck());
        cv.put("img", BitMaptoBtye(data.getImg()));
        handler.update(DummyTable.getTABLE_NAME(), cv, "id="+ data.getId(), null);
        handler.close();
    }

    public static boolean deleteDummyTableById( int id ,Context context){
        handler = new DatabaseHandler(context).getWritableDatabase();
        return handler.delete(DummyTable.getTABLE_NAME(), "id =" + id, null) > 0;
    }

    public static boolean deleteDummyTableByDescription(String description, Context context){
        handler = new DatabaseHandler(context).getWritableDatabase();
        return handler.delete(DummyTable.getTABLE_NAME(), "description LIKE %" + description + "%", null) > 0;
    }

    private static Bitmap byteToBitMap(byte[] img) {
        return BitmapFactory.decodeByteArray(img, 0, img.length);
    }

    private static byte[] BitMaptoBtye(Bitmap bm) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        }


}
