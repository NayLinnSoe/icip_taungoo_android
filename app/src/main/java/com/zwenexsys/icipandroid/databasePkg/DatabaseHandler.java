package com.zwenexsys.icipandroid.databasePkg;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by invictus on 5/19/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String DB_NAME = "MovieRepo";
    private static final int DB_VERSION = 1;

    public DatabaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + DummyTable.getTABLE_NAME() + "("
                + DummyTable.getTableColumn() + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DummyTable.getTABLE_NAME());
        onCreate(db);
    }
}
