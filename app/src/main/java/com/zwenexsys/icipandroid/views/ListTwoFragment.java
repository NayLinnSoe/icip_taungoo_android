package com.zwenexsys.icipandroid.views;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.Api;
import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.Session.SessionManager;
import com.zwenexsys.icipandroid.adapter.ListRecyclerAdapter;
import com.zwenexsys.icipandroid.events.CityEvent;
import com.zwenexsys.icipandroid.rest.ApiClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by mt on 5/19/17.
 */

public class ListTwoFragment extends Fragment {
    ApiClient apiClient;
    ListRecyclerAdapter adapter;
    RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiClient = ApiClient.getApiClientInstance();
        adapter = new ListRecyclerAdapter(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_two, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.smoothScrollToPosition(0);
        recyclerView.setHasFixedSize(true);

        apiClient.getCity(SessionManager.getSessionToken(getContext()));
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void getCity(CityEvent event) {
        if (event.isSuccess()) {
            adapter.addItem(event.getCity());
            recyclerView.setAdapter(adapter);
        }
    }

}