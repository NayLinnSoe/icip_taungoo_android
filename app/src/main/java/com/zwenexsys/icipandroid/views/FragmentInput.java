package com.zwenexsys.icipandroid.views;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.adapter.InputListViewAdapter;
import com.zwenexsys.icipandroid.databaseInput.InputDatabaseManager;
import com.zwenexsys.icipandroid.databaseInput.InputTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mt on 5/19/17.
 */

public class FragmentInput extends Fragment {

    //initializedb
    final InputDatabaseManager inputDatabaseManager = InputDatabaseManager.initDatabaseManager();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Input Sample");

        //mapping
        final View v = inflater.inflate(R.layout.input_fragment, container, false);
        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        final ListView listView = (ListView) v.findViewById(R.id.listView);
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.input_swipe_refresh_layout);
        final Spinner spinner = (Spinner) v.findViewById(R.id.spinner);

        //spinner
        setSpinner(spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                String spinnerTitle = spinner.getSelectedItem().toString();

                //show all list if nothing is selected
                if (spinnerTitle.equalsIgnoreCase(".....")) {
                    showInputList(listView);
                } else {
                    //show selected title
                    showInputList(spinnerTitle, listView);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        //show list
        showInputList(listView);

        //show input dialog in floating button click
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View editView = LayoutInflater.from(getActivity()).inflate(R.layout.view_input, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                final EditText title = (EditText) editView.findViewById(R.id.title);
                final EditText content = (EditText) editView.findViewById(R.id.content);

                final AlertDialog dialog = builder.create();
                dialog.setView(editView);
                dialog.show();

                LinearLayout okLayout = (LinearLayout) editView.findViewById(R.id.enter);
                okLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //prepare data
                        String titleString = title.getText().toString();
                        String contentString = content.getText().toString();
                        InputTable inputTable = new InputTable(titleString, contentString);

                        //save to database
                        saveInput(inputTable);
                        //set to spinner
                        setSpinner(spinner);
                        Toast.makeText(getContext(), "added", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
            }
        });


        //implement refresh listener to refresh list
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                String spinnerTitle = spinner.getSelectedItem().toString();
                if (spinnerTitle.equalsIgnoreCase(".....")) {
                    showInputList(listView);
                } else {
                    showInputList(spinnerTitle, listView);
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        return v;
    }

    //show all list
    public void showInputList(ListView listView) {

        //get list


        List<InputTable> inputList = getInputList();
        List<String> title = new ArrayList<>();
        List<String> content = new ArrayList<>();

        for (int i = 0; i < inputList.size(); i++) {
            title.add(inputList.get(i).getTitle().toString());
            content.add(inputList.get(i).getContent());
        }

        //adapter
        InputListViewAdapter inputListViewAdapter;
        inputListViewAdapter = new InputListViewAdapter(getContext(), title, content);
        listView.setAdapter(inputListViewAdapter);

    }

    //show selected title
    public void showInputList(String spinnerTitle, ListView listView) {
        List<InputTable> inputList = getInputList();
        List<String> title = new ArrayList<>();
        List<String> content = new ArrayList<>();

        for (int i = 0; i < inputList.size(); i++) {
            if (spinnerTitle.equalsIgnoreCase(inputList.get(i).getTitle().toString())) {
                title.add(inputList.get(i).getTitle().toString());
                content.add(inputList.get(i).getContent());

            }
        }

        //adapter
        InputListViewAdapter inputListViewAdapter;
        inputListViewAdapter = new InputListViewAdapter(getContext(), title, content);
        listView.setAdapter(inputListViewAdapter);


    }

    //set spinner from database
    public void setSpinner(Spinner spinner) {
        List<String> title = getTitle();

        ArrayList<String> titleArray = new ArrayList<>();
        titleArray.add(".....");
        ArrayAdapter<String> arrayAdapter;

        for (int i = 0; i < title.size(); i++) {
            titleArray.add(title.get(i).toString());
        }


        arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, titleArray);
        spinner.setAdapter(arrayAdapter);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    //database processes
    public List<InputTable> getInputList() {
        List<InputTable> inputList = inputDatabaseManager.getInstances(getContext());
        return inputList;
    }

    public List<String> getTitle() {
        List<String> title = inputDatabaseManager.getTitle(getContext());
        return title;
    }

    public void saveInput(InputTable inputTable) {
        //save to database
        inputDatabaseManager.saveInputTable(inputTable, getContext());
    }

}