package com.zwenexsys.icipandroid.views;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zwenexsys.icipandroid.BuildConfig;
import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.databinding.ActivityAboutBinding;
import com.zwenexsys.icipandroid.util.Intents;

/**
 * Created by user on 8/23/16.
 */

public class AboutFragment extends Fragment implements View.OnClickListener {
  ActivityAboutBinding activityAboutBinding;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    getActivity().setTitle("About");
    View view = inflater.inflate(R.layout.activity_about, container, false);
    activityAboutBinding = DataBindingUtil.bind(view);


    PackageManager pm = getContext().getPackageManager();
    String packageName = getContext().getApplicationContext().getPackageName();
    String versionName = null;
    try {
      assert pm != null;
      PackageInfo info = pm.getPackageInfo(packageName, 0);
      versionName = info.versionName;
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    }

    activityAboutBinding.version.setText(BuildConfig.VERSION_NAME);
    // About app
    activityAboutBinding.yoteshinAppDescription.setText(
        Html.fromHtml("<center>" + getString(R.string.about_app) + "</center>"));

    // Rate Us

    activityAboutBinding.phone.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        callIntent.setData(Uri.parse("tel:01393082"));
        Intents.maybeStartActivity(getContext(), callIntent);
      }
    });

    activityAboutBinding.fb.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intents.maybeStartActivity(getContext(), getOpenFacebookIntent(getContext()));
      }
    });

    activityAboutBinding.email.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          Intent emailIntent =
              new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "contact@zwenex.com", null));
          emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Hello! : YoteShin");
          Intents.maybeStartActivity(getContext(),
              Intent.createChooser(emailIntent, "Send email..."));
        } catch (ActivityNotFoundException anfe) {
          Toast.makeText(getContext(), "No connection", Toast.LENGTH_SHORT).show();
        }
      }
    });

    //AnalyticManager.sendScreenView("About Screen");
    activityAboutBinding.zwenexLogo.setOnClickListener(this);

    return view;
  }

  @Override
  public void onClick(View view) {
    int i = view.getId();
    Uri uri;
    Intent intent;
    switch (i) {
      case R.id.zwenex_logo:
        uri = Uri.parse("http://www.zwenex.com/");
        intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
        //AnalyticManager.sendEvent("btn.click", "about.zwenex_logo", "zwenex_logo");
        break;
    }
  }

  private Intent getOpenFacebookIntent(Context context) {
    try {
      context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
      try {
        return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/612481822134847"));
      } catch (android.content.ActivityNotFoundException e) {
        return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/yoteshin"));
      }
    } catch (Exception e) {
      return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/yoteshin"));
    }
  }
}

