package com.zwenexsys.icipandroid.views;


import android.content.Context;
import android.os.Bundle;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.Session.SessionManager;
import com.zwenexsys.icipandroid.adapter.ListAdapterOne;
import com.zwenexsys.icipandroid.events.TeamEvent;
import com.zwenexsys.icipandroid.rest.ApiClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by mt on 5/19/17.
 */

public class ListOneFragment extends Fragment {
    ListAdapterOne adapter;
    ApiClient apiClient;
    ListView listView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiClient = ApiClient.getApiClientInstance();
        adapter = new ListAdapterOne();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_list, container, false);
        listView = (ListView) v.findViewById(R.id.listView);
        apiClient.getTeam(SessionManager.getSessionToken(getContext()));
        return v;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void teamEvent(TeamEvent event) {
        if (event.isSuccess()) {
            adapter.setTeamList(event.getTeam());
            listView.setAdapter(adapter);
        }
    }
}

