package com.zwenexsys.icipandroid.dropDownPkg;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.zwenexsys.icipandroid.R;

import java.util.ArrayList;
import java.util.List;

public class SpinnerActivity extends Activity implements AdapterView.OnItemSelectedListener{

    private static List<String> data;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        generateDummySpinnerData();

        spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(SpinnerActivity.this,
                android.R.layout.simple_spinner_item,data);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


    }



    public void generateDummySpinnerData() {
        data = new ArrayList<>();
        for(int i=0; i<10; i++) {
            data.add("SpinnerItem " + i);
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

        Snackbar.make(parent,  Html.fromHtml("<font color=\"#ffffff\">Tap to open</font>"), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
