package com.zwenexsys.icipandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.sax.TextElementListener;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.Session.SessionManager;
import com.zwenexsys.icipandroid.util.CheckVersion;
import com.zwenexsys.icipandroid.views.AboutFragment;
import com.zwenexsys.icipandroid.views.FragmentInput;
import com.zwenexsys.icipandroid.views.FragmentList;
import com.zwenexsys.icipandroid.views.FragmentSearch;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new CheckVersion(getApplicationContext(), "http://apps.zwenex.com/dl/iciptaungoo").execute();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (!TextUtils.isEmpty(SessionManager.getUserName(getApplicationContext()))) {
            View view = navigationView.inflateHeaderView(R.layout.nav_header_navigation);
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(SessionManager.getUserName(getApplicationContext()));
        }
        FragmentList list = new FragmentList();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, list).commit();
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_list) {
            FragmentList list = new FragmentList();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, list).commit();
        } else if (id == R.id.nav_input) {
            FragmentInput input = new FragmentInput();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, input).commit();
        } else if (id == R.id.nav_search) {
            FragmentSearch search = new FragmentSearch();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, search).commit();
        } else if (id == R.id.about) {
            AboutFragment aboutFragment = new AboutFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, aboutFragment).commit();
        } else if (id == R.id.logout) {
            SessionManager.resetData(getApplicationContext());
            Intent intent = new Intent(NavigationActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
