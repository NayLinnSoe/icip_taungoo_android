package com.zwenexsys.icipandroid.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.Session.SessionManager;
import com.zwenexsys.icipandroid.events.DetailTeamEvent;
import com.zwenexsys.icipandroid.model.Team;
import com.zwenexsys.icipandroid.rest.ApiClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class TeamDetailActivity extends AppCompatActivity {
    private static final String ITEM_ID = "team_id";
    ApiClient apiclient;
    TextView teamName;
    TextView memOne;
    TextView memTwo;
    TextView memThhree;
    TextView memFour;
    TextView memFive;

    public static Intent intentInstance(Context context, String id) {
        Intent intent = new Intent(context, TeamDetailActivity.class);
        intent.putExtra(ITEM_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_detail);
        setTitle("Team's Detail");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        teamName = (TextView) findViewById(R.id.team_name);
        memOne = (TextView) findViewById(R.id.mName);
        memTwo = (TextView) findViewById(R.id.mNametwo);
        memThhree = (TextView) findViewById(R.id.mNamethree);
        memFour = (TextView) findViewById(R.id.mNamefour);
        memFive = (TextView) findViewById(R.id.mNamefive);
        apiclient = ApiClient.getApiClientInstance();
        if (getIntent().hasExtra(ITEM_ID)) {
            apiclient.detailTeam(getIntent().getStringExtra(ITEM_ID), SessionManager.getSessionToken(getApplicationContext()));
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void detailEvent(DetailTeamEvent event) {
        if (event.isSuccess()) {
            teamName.setText(event.getDetailTeam().getTeamName());
            if (event.getDetailTeam().getStudents().size() == 4) {
                memOne.setText(event.getDetailTeam().getStudents().get(0).getStudentName());
                memTwo.setText(event.getDetailTeam().getStudents().get(1).getStudentName());
                memThhree.setText(event.getDetailTeam().getStudents().get(2).getStudentName());
                memFour.setText(event.getDetailTeam().getStudents().get(3).getStudentName());
            } else {
                memOne.setText(event.getDetailTeam().getStudents().get(0).getStudentName());
                memTwo.setText(event.getDetailTeam().getStudents().get(1).getStudentName());
                memThhree.setText(event.getDetailTeam().getStudents().get(2).getStudentName());
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
