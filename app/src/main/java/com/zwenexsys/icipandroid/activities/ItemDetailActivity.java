package com.zwenexsys.icipandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by nalioes on 5/21/17.
 */

public class ItemDetailActivity extends AppCompatActivity {
    private final String ITEM_ID = "item_id";

    public Intent intentInstance(String id) {
        Intent intent = new Intent(ItemDetailActivity.this, ItemDetailActivity.class);
        intent.putExtra(ITEM_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

}
