package com.zwenexsys.icipandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.Session.SessionManager;
import com.zwenexsys.icipandroid.events.LoginEvent;
import com.zwenexsys.icipandroid.rest.ApiClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {
    ApiClient apiClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button loginBtn = (Button) findViewById(R.id.btn_login);
        apiClient = ApiClient.getApiClientInstance();
        final EditText username = (EditText) findViewById(R.id.et_username);
        final EditText password = (EditText) findViewById(R.id.et_password);

        if (!TextUtils.isEmpty(SessionManager.getSessionToken(getApplicationContext()))) {
            Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
            startActivity(intent);
            finish();
        }
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText())) {
                    apiClient.login(username.getText().toString(), password.getText().toString());
                } else if (!TextUtils.isEmpty(username.getText())) {
                    Toast.makeText(MainActivity.this, "Enter email", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Enter password", Toast.LENGTH_SHORT).show();
                }


            }
        });
        /*
        // use the followwing code to exercise the recycler view codes.

        Intent i = new Intent(this, recyclerActivity.class);
        startActivity(i);*/
        /*
        //use the following code to exercise input and display codes.

        Intent i = new Intent(this, inputDisplayActivity.class);
        startActivity(i);*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void login(LoginEvent event) {
        if (event.isSuccess()) {
            SessionManager.setUserName(getApplicationContext(), event.getLoginUser().getUser_name());
            SessionManager.setSessionToken(getApplicationContext(), event.getLoginUser().getSession_key());
            Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Invalid email or password", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
