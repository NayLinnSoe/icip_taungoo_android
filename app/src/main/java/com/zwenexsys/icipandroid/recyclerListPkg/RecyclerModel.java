package com.zwenexsys.icipandroid.recyclerListPkg;

/**
 * Created by invictus on 5/19/17.
 */

public class RecyclerModel {

    private String title;
    private String description;

    public RecyclerModel () {}

    public RecyclerModel(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
