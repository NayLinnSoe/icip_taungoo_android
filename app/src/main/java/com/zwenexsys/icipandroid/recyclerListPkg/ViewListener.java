package com.zwenexsys.icipandroid.recyclerListPkg;

/**
 * Created by invictus on 5/19/17.
 */

public interface ViewListener {
    void onReceivedView(int model);
}
