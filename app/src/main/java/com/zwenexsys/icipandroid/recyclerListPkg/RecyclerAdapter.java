package com.zwenexsys.icipandroid.recyclerListPkg;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.zwenexsys.icipandroid.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by invictus on 5/19/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{

    static ViewListener listener;
    private static List<RecyclerModel> dataList;
    private static Context context;

    static {
        dataList = new ArrayList<>();
    }

    public RecyclerAdapter(Context context, List<RecyclerModel> list) {
        RecyclerAdapter.context = context;
        dataList = list;
    }

    public void setListener(ViewListener vListener) {
        listener = vListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_list_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.title.setText(dataList.get(position).getTitle());
        holder.description.setText(dataList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView description;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onReceivedView(getAdapterPosition());
                    }

                }
            });

        }
    }

}
