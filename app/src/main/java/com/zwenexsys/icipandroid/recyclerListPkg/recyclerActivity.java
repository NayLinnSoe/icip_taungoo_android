package com.zwenexsys.icipandroid.recyclerListPkg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.zwenexsys.icipandroid.R;

import java.util.ArrayList;
import java.util.List;

/*
    this is the recycler view activity

    add the following code to  app->build.gradle
    // for recycler view
    compile 'com.android.support:recyclerview-v7:25.3.0'
    compile 'com.android.support:design:25.3.0'

    and sync the project

 */



public class recyclerActivity extends Activity {

    private RecyclerView recycler;
    private static List<RecyclerModel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        generateDummyData();
        setContentView(R.layout.activity_recycler);
        recycler = (RecyclerView) findViewById(R.id.recycler);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        RecyclerAdapter adapter = new RecyclerAdapter(this, data);
        adapter.setListener(new ViewListener() {
            @Override
            public void onReceivedView(int data) {


                Intent i = new Intent(recyclerActivity.this, RecyclerDetail.class);
                i.putExtra("position", data+"");
                startActivity(i);

            }
        });
        recycler.setAdapter(adapter);



    }

    public void generateDummyData() {
        data = new ArrayList<>();
        String desc = "Description duplication. ";
        for(int i=0; i<10; i++, desc += desc) {
            data.add(new RecyclerModel("Title "+ i, desc + "Total "+ i ));
        }

    }

    public static RecyclerModel getData(int position) {

        return data.get(position);
    }

}
