package com.zwenexsys.icipandroid.recyclerListPkg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.zwenexsys.icipandroid.R;

public class RecyclerDetail extends Activity {

    private TextView title;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_detail);

        Intent intent = getIntent();
        int position = Integer.parseInt(intent.getStringExtra("position"));

        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);

        RecyclerModel rm = recyclerActivity.getData(position);
        title.setText(rm.getTitle());
        description.setText(rm.getDescription());
    }
}
