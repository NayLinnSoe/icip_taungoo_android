package com.zwenexsys.icipandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nalioes on 5/22/17.
 */

public class LoginUser {
    @SerializedName("session_key")
    @Expose
    private String session_key;
    @SerializedName("user_name")
    @Expose
    private String user_name;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSession_key() {
        return session_key;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getMessage() {
        return message;
    }
}
