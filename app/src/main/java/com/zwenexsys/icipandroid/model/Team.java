package com.zwenexsys.icipandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nalioes on 5/22/17.
 */

public class Team {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("team_name")
    @Expose
    private String team_name;
    @SerializedName("created_at")
    @Expose
    private String created_at;

    public Integer getId() {
        return id;
    }

    public String getTeam_name() {
        return team_name;
    }

    public String getCreated_at() {
        return created_at;
    }
}
