package com.zwenexsys.icipandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nalioes on 5/22/17.
 */

public class Student {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("student_name")
    @Expose
    public String studentName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("team_id")
    @Expose
    public Integer teamId;
    @SerializedName("team_name")
    @Expose
    public String teamName;
    @SerializedName("phone_no")
    @Expose
    public String phoneNo;
    @SerializedName("original_image1_url")
    @Expose
    public String originalImage1Url;
    @SerializedName("medium_image1_url")
    @Expose
    public String mediumImage1Url;

    public Integer getId() {
        return id;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getEmail() {
        return email;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getOriginalImage1Url() {
        return originalImage1Url;
    }

    public String getMediumImage1Url() {
        return mediumImage1Url;
    }
}
