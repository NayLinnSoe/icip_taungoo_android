package com.zwenexsys.icipandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nalioes on 5/22/17.
 */

public class City {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("images")
    @Expose
    private String images;

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getImages() {
        return images;
    }
}
