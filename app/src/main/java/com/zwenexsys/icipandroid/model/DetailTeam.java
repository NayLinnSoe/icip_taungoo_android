package com.zwenexsys.icipandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nalioes on 5/22/17.
 */

public class DetailTeam {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("team_name")
    @Expose
    public String teamName;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("students")
    @Expose
    public List<Student> students = null;

    public Integer getId() {
        return id;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public List<Student> getStudents() {
        return students;
    }
}
