package com.zwenexsys.icipandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.model.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nalioes on 5/21/17.
 */

public class ListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<City> cities = new ArrayList<>();
    Context context;

    public ListRecyclerAdapter(Context context) {
        this.context = context;
    }

    public void addItem(List<City> cities) {
        this.cities = cities;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        City city = cities.get(position);
        ((ItemViewHolder) holder).title.setText(city.getTitle());
        ((ItemViewHolder) holder).content.setText(city.getDesc());
        Glide.with(context)
                .load(city.getImages())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((ItemViewHolder) holder).image);
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView content;
        private ImageView image;

        public ItemViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            content = (TextView) itemView.findViewById(R.id.content);
            image = (ImageView) itemView.findViewById(R.id.imaSE);

        }

    }
}
