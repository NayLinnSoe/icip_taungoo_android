package com.zwenexsys.icipandroid.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.activities.TeamDetailActivity;
import com.zwenexsys.icipandroid.model.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nalioes on 5/21/17.
 */

public class ListAdapterOne extends BaseAdapter {
    List<Team> teamList = new ArrayList<>();

    public ListAdapterOne() {

    }

    public void setTeamList(List<Team> teamList) {
        this.teamList = teamList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return teamList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, final ViewGroup viewGroup) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_team, viewGroup, false);
        TextView title = (TextView) itemView.findViewById(R.id.title);
        LinearLayout viewTeam = (LinearLayout) itemView.findViewById(R.id.team_view);
        final Team team = teamList.get(i);
        title.setText(team.getTeam_name());
        viewTeam.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = TeamDetailActivity.intentInstance(viewGroup.getContext(), String.valueOf(team.getId()));
                        viewGroup.getContext().startActivity(intent);
                    }
                }
        );
        return itemView;
    }
}
