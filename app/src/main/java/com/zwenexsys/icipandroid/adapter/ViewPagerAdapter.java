package com.zwenexsys.icipandroid.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.zwenexsys.icipandroid.views.ListOneFragment;
import com.zwenexsys.icipandroid.views.ListTwoFragment;
import com.zwenexsys.icipandroid.util.SmartFragmentStatePagerAdapter;

/**
 * Created by nls on 3/6/17.
 */

public class ViewPagerAdapter extends SmartFragmentStatePagerAdapter {

    FragmentManager fragmentManager;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentManager = fm;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ListOneFragment();
            case 1:
                return new ListTwoFragment();
            case 2:
//                return new SmartFragment();
            case 3:
//                return new AccessoriesFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
