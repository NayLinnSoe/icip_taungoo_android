package com.zwenexsys.icipandroid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zwenexsys.icipandroid.R;
import com.zwenexsys.icipandroid.views.FragmentInput;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mt on 5/20/17.
 */

public class InputListViewAdapter extends BaseAdapter {

    public List<String> title = new ArrayList<>();
    public List<String> content = new ArrayList<>();
    public Context context;
    public LayoutInflater inflater;

    public InputListViewAdapter(Context context, List<String> title, List<String> content) {
        super();

        this.context = context;
        this.title = title;
        this.content = content;

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item_input, null);


            holder.txtTitle = (TextView) convertView.findViewById(R.id.txttitle);
            holder.txtContent = (TextView) convertView.findViewById(R.id.txtcontent);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();


        holder.txtTitle.setText(title.get(position).toString());
        holder.txtContent.setText(content.get(position).toString());

        return convertView;
    }

    public static class ViewHolder {

        TextView txtTitle;
        TextView txtContent;
    }
}
