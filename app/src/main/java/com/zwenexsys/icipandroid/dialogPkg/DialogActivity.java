package com.zwenexsys.icipandroid.dialogPkg;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zwenexsys.icipandroid.R;

import java.util.ArrayList;

public class DialogActivity extends Activity {

    private Button clickMe;
    private Button ok;
    private Button cancel;
    private TextView title;
    private EditText input;
    private AlertDialog.Builder builder;
    private LinearLayout layout;
    private  AlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        clickMe = (Button) findViewById(R.id.showDialog);

        clickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeDialog();
            }
        });

    }

    public void makeDialog() {
        builder = new android.support.v7.app.AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_custom_layout, null);
        ok = (Button) v.findViewById(R.id.next);
        cancel = (Button) v.findViewById(R.id.cancel);
        input = (EditText) v.findViewById(R.id.input);
        title = (TextView) v.findViewById(R.id.title);
        layout = (LinearLayout) v.findViewById(R.id.linear);

        title.setText("Custom Layout Title");
        builder.setView(v);
        builder.create();
        dialog = builder.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(input.getText())) {

                    Toast.makeText(DialogActivity.this, "HAHA you can only enter numbers. check the code :D", Toast.LENGTH_SHORT).show();

                }else {
                    cancel.setText("Got It");
                    ok.setVisibility(View.GONE);
                    layout.setWeightSum(1);
                    input.setVisibility(View.GONE);
                    title.setText("CONGRATULATIONS!!!");
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.equals("Got It", cancel.getText())) {
                    Toast.makeText(DialogActivity.this, "Nice. :D", Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }
        });

    }


}
