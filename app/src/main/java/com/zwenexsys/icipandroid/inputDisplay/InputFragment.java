package com.zwenexsys.icipandroid.inputDisplay;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zwenexsys.icipandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputFragment extends Fragment{

    private static EditText input;
    private Button go;

    public InputFragment() {
        // Required empty public constructor
    }

    public static String getText() {
        return input.getText().toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment_list
        View v= inflater.inflate(R.layout.fragment_input, container, false);
        input = (EditText) v.findViewById(R.id.inputText);
        go = (Button) v.findViewById(R.id.go);

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DisplayFragment frag2 = new DisplayFragment();
                getFragmentManager().beginTransaction().replace(R.id.display, frag2).commit();

            }
        });

        return v;
    }

}
