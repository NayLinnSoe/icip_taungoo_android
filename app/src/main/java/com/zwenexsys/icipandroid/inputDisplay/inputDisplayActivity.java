package com.zwenexsys.icipandroid.inputDisplay;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.zwenexsys.icipandroid.R;

public class inputDisplayActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_display);

        InputFragment frag1 = new InputFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.input, frag1).commit();

    }
}
