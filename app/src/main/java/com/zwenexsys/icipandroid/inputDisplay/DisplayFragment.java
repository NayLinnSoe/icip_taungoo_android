package com.zwenexsys.icipandroid.inputDisplay;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zwenexsys.icipandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DisplayFragment extends Fragment{

    private TextView data;

    public DisplayFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment_list
        View v = inflater.inflate(R.layout.fragment_display, container, false);
        data = (TextView) v.findViewById(R.id.display);
        data.setText(InputFragment.getText());
        return v;
    }

}
