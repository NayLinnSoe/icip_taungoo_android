package com.zwenexsys.icipandroid.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;

import com.zwenexsys.icipandroid.BuildConfig;
import com.zwenexsys.icipandroid.R;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by user on 8/29/16.
 */

public class CheckVersion extends AsyncTask<Void, Void, String>

{
    private static final String GooglePlayStorePackageNameOld = "com.google.market";
    private static final String GooglePlayStorePackageNameNew = "com.android.vending";
    private static final String DOWNLOAD_URL = "http://apps.zwenex.com/dl/iciptaungoo";
    private static Dialog dialog;
    private String ENDPOINT;
    private Context mContext;
    DialogInterface.OnClickListener acceptDialogListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            switch (i) {
                case DialogInterface.BUTTON_POSITIVE:
                    Uri uri = Uri.parse(DOWNLOAD_URL);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    mContext.startActivity(intent);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };
    private String responseString;

    public CheckVersion(Context context, String appName) {
        this.ENDPOINT = appName;
        this.mContext = context;
    }

    private String checkVersion() {
        OkHttpClient okHttpClient = new OkHttpClient();

        // Create request for remote resource.
        Request request = new Request.Builder().url(ENDPOINT).build();

        // Execute the request and retrieve the response.
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.code() == 200) {
                return response.body().string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mContext);
        builder.setMessage(mContext.getString(R.string.new_version_body))
                .setTitle(mContext.getString(R.string.new_version_title))
                .setPositiveButton(android.R.string.ok, acceptDialogListener)
                .setNegativeButton(android.R.string.cancel, acceptDialogListener);
        dialog = builder.create();

    }

    @Override
    protected String doInBackground(Void... voids) {
        return checkVersion();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s.equals("")) {

        } else {
            try {
                Integer value = Integer.parseInt(s);

                if (value > BuildConfig.VERSION_CODE) {
                    dialog.show();

                } else {

                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isPlayStoreInstalled() {
        PackageManager packageManager = mContext.getPackageManager();
        List<PackageInfo> packages =
                packageManager.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);
        for (PackageInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(GooglePlayStorePackageNameOld)
                    || packageInfo.packageName.equals(GooglePlayStorePackageNameNew)) {
                return true;
            }
        }

        return false;
    }

}
