package com.zwenexsys.icipandroid.events;

import com.zwenexsys.icipandroid.model.City;
import com.zwenexsys.icipandroid.model.DetailTeam;

import java.util.List;

/**
 * Created by nls on 3/13/17.
 */

public class DetailTeamEvent {
    private boolean success;
    private DetailTeam detailTeam;

    public DetailTeamEvent(boolean success) {
        this.success = success;
    }

    public DetailTeamEvent(boolean success, DetailTeam detailTeam) {
        this.success = success;
        this.detailTeam = detailTeam;
    }

    public boolean isSuccess() {
        return success;
    }

    public DetailTeam getDetailTeam() {
        return detailTeam;
    }
}
