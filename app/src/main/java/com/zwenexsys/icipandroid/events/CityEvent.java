package com.zwenexsys.icipandroid.events;

import com.zwenexsys.icipandroid.model.City;
import com.zwenexsys.icipandroid.model.LoginUser;

import java.util.List;

/**
 * Created by nls on 3/13/17.
 */

public class CityEvent {
    private boolean success;
    private List<City> city;

    public CityEvent(boolean success) {
        this.success = success;
    }

    public CityEvent(boolean success, List<City> city) {
        this.success = success;
        this.city = city;
    }

    public boolean isSuccess() {
        return success;
    }

    public List<City> getCity() {
        return city;
    }
}
