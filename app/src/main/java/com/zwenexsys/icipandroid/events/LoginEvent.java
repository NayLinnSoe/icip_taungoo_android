package com.zwenexsys.icipandroid.events;

import com.zwenexsys.icipandroid.model.LoginUser;

/**
 * Created by nls on 3/13/17.
 */

public class LoginEvent {
    private boolean success;
    private LoginUser loginUser;

    public LoginEvent(boolean success) {
        this.success = success;
    }

    public LoginEvent(boolean success, LoginUser loginUser) {
        this.success = success;
        this.loginUser = loginUser;
    }

    public boolean isSuccess() {
        return success;
    }

    public LoginUser getLoginUser() {
        return loginUser;
    }
}
