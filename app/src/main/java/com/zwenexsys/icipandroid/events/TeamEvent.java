package com.zwenexsys.icipandroid.events;

import com.zwenexsys.icipandroid.model.LoginUser;
import com.zwenexsys.icipandroid.model.Team;

import java.util.List;

/**
 * Created by nls on 3/13/17.
 */

public class TeamEvent {
    private boolean success;
    private List<Team> team;

    public TeamEvent(boolean success) {
        this.success = success;
    }

    public TeamEvent(boolean success, List<Team> team) {
        this.success = success;
        this.team = team;
    }

    public boolean isSuccess() {
        return success;
    }

    public List<Team> getTeam() {
        return team;
    }
}
