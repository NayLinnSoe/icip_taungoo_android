package com.zwenexsys.icipandroid.rest;

import com.zwenexsys.icipandroid.events.CityEvent;
import com.zwenexsys.icipandroid.events.DetailTeamEvent;
import com.zwenexsys.icipandroid.events.LoginEvent;
import com.zwenexsys.icipandroid.events.TeamEvent;
import com.zwenexsys.icipandroid.model.City;
import com.zwenexsys.icipandroid.model.DetailTeam;
import com.zwenexsys.icipandroid.model.LoginUser;
import com.zwenexsys.icipandroid.model.Team;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 */

public class ApiClient implements ClientAccess {

    public static String BASE_URL = "http://139.59.110.125/";
    private static ApiClient apiClient;
    private static Retrofit retrofit = null;
    private static OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request.Builder builder = originalRequest.newBuilder().addHeader("Authorization", Credentials.basic("admin", "123"));
            Request newRequest = builder.build();
            return chain.proceed(newRequest);
        }
    }).build();
    private ApiInterface apiInterface;


    private ApiClient() {
        apiInterface = getClient().create(ApiInterface.class);
    }

    public static ApiClient getApiClientInstance() {
        if (apiClient == null) {
            apiClient = new ApiClient();
        }
        return apiClient;
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }

    @Override
    public void login(String email, String password) {
        Call<LoginUser> call = apiInterface.login(email, password);
        call.enqueue(new Callback<LoginUser>() {
            @Override
            public void onResponse(Call<LoginUser> call, Response<LoginUser> response) {
                if (response.body() != null && response.body().getMessage() == null) {
                    EventBus.getDefault().post(new LoginEvent(true, response.body()));
                } else {
                    EventBus.getDefault().post(new LoginEvent(false, response.body()));
                }
            }

            @Override
            public void onFailure(Call<LoginUser> call, Throwable t) {
                EventBus.getDefault().post(new LoginEvent(false));
            }
        });
    }

    @Override
    public void getCity(String session) {
        Call<List<City>> call = apiInterface.getCity(session);
        call.enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(Call<List<City>> call, Response<List<City>> response) {
                if (response.body() != null) {
                    EventBus.getDefault().post(new CityEvent(true, response.body()));
                } else {
                    EventBus.getDefault().post(new CityEvent(false));
                }
            }

            @Override
            public void onFailure(Call<List<City>> call, Throwable t) {
                EventBus.getDefault().post(new CityEvent(false));
            }
        });
    }

    @Override
    public void getTeam(String session) {
        Call<List<Team>> call = apiInterface.getTeam(session);
        call.enqueue(new Callback<List<Team>>() {
            @Override
            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                if (response.body() != null) {
                    EventBus.getDefault().post(new TeamEvent(true, response.body()));
                } else {
                    EventBus.getDefault().post(new TeamEvent(false));
                }
            }

            @Override
            public void onFailure(Call<List<Team>> call, Throwable t) {
                EventBus.getDefault().post(new TeamEvent(false));

            }
        });
    }

    @Override
    public void detailTeam(String id, String session) {
        Call<DetailTeam> detail = apiInterface.detailTeam(id, session);
        detail.enqueue(new Callback<DetailTeam>() {
            @Override
            public void onResponse(Call<DetailTeam> call, Response<DetailTeam> response) {
                if (response.body() != null) {
                    EventBus.getDefault().post(new DetailTeamEvent(true, response.body()));
                } else {
                    EventBus.getDefault().post(new DetailTeamEvent(false));
                }
            }

            @Override
            public void onFailure(Call<DetailTeam> call, Throwable t) {
                EventBus.getDefault().post(new DetailTeamEvent(false));
            }
        });
    }
}
