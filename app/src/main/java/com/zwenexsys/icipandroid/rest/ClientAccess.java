package com.zwenexsys.icipandroid.rest;

/**
 *
 */

public interface ClientAccess {
    void login(String email, String password);

    void getCity(String session);

    void getTeam(String session);

    void detailTeam(String id, String session);
}
