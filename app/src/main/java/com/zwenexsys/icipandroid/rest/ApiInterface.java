package com.zwenexsys.icipandroid.rest;


import com.zwenexsys.icipandroid.model.City;
import com.zwenexsys.icipandroid.model.DetailTeam;
import com.zwenexsys.icipandroid.model.LoginUser;
import com.zwenexsys.icipandroid.model.Team;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 *
 */

public interface ApiInterface {

    @POST("api/v1/authenticate?")
    Call<LoginUser> login(@Query("email") String email, @Query("password") String password);

    @GET("api/v1/cities?")
    Call<List<City>> getCity(@Query("session_key") String session);

    @GET("api/v1/teams?")
    Call<List<Team>> getTeam(@Query("session_key") String session);

    @GET("api/v1/team_details/{id}")
    Call<DetailTeam> detailTeam(@Path("id") String id, @Query("session_key") String session);

}
